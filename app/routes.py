from flask import render_template, flash, redirect, url_for, request
from app import app, db, mail
from app.forms import LoginForm, RegisterationForm, ForgotForm, ResetPasswordForm, EmptyForm
from app.models import User
from werkzeug.urls import url_parse
from datetime import datetime
from flask_login import current_user, login_user, logout_user, login_required
from flask_mail import Message
from app.email import send_reset_email


@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('user',first_name=current_user.first_name))   
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:
            flash('کاربری با این ایمیل وجود ندارد - لطفا ابتدا ثبت نام کنید')
            return redirect(url_for('login'))
        elif user and not user.check_password(form.password.data):
            flash('رمز عبور صحیح نیست')
            return redirect(url_for('login'))
        login_user(user)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('user', first_name=current_user.first_name)
        return redirect(next_page)
    return render_template('login.html', title='ورود به حساب کاربری', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('user',first_name=current_user.first_name))
    form = RegisterationForm()
    if form.validate_on_submit():
        user = User(
            first_name=form.first_name.data,
            last_name=form.last_name.data, 
            email=form.email.data,
            phone=form.phone.data
            )
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('ثبت نام با موفقیت انجام شد :)')
        return redirect(url_for('login'))
    return render_template('register.html', title='ثبت نام', form=form)


@app.route('/user/<first_name>')
@login_required
def user(first_name):
    user = User.query.filter_by(first_name=first_name).first_or_404()
    form = EmptyForm()
    return render_template('user.html', user=user,form=form)


@app.route('/forgot', methods=['GET', 'POST'])
def forgot():
    if current_user.is_authenticated:
        return redirect(url_for('user', first_name=current_user.first_name)) 
    form = ForgotForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if not user:
            flash('کاربری با این ایمیل وجود ندارد ')
            return redirect(url_for('forgot'))
        send_reset_email(user)
        flash('لینک ارسال شد - لطفا ایمیل خود را چک کنید  ')
        return redirect(url_for('login'))
    return render_template('forgot.html', form=form)


@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('user', first_name=current_user.first_name)) 
    user = User.verify_reset_token(token)
    if not user:
        flash('لینک منقضی شده است')
        return redirect(url_for('forgot'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('رمز عبور شما تغییر یافت')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form, title='تغییر رمز')
