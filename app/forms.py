import phonenumbers
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField,SubmitField,TextAreaField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo,Length
from app.models import User


class LoginForm(FlaskForm): 
    email = StringField(
        'ایمیل:', validators=[DataRequired(message='لطفا ایمیل خود را وارد کنید'),
        Email(check_deliverability=True,message='ایمیل معتبر نیست')])
    password = PasswordField(
        'رمز عبور:', validators=[DataRequired(
        message='لطفا رمز خود را وارد کنید'),Length(min=6,max=12,
        message='''رمز عبور باید بین ۶ تا ۱۲ کاراکتر طول داشته باشد''')])
    submit = SubmitField('ورود')

    
class RegisterationForm(FlaskForm):
    first_name = StringField(
        'نام:', validators=[DataRequired(message='لطفا نام خود را وارد کنید')])
    last_name = StringField(
        'نام خانوادگی:', validators=[DataRequired(message=
        'لطفا نام خانوادگی خود را وارد کنید')])
    email = StringField(
        'ایمیل:', validators=[DataRequired(message=
        'لطفا ایمیل خود را وارد کنید'),
        Email(check_deliverability=True,message='ایمیل معتبر نیست')])
    phone = StringField(
        'شماره موبایل:', validators=[DataRequired(
        message='لطفا شماره تماس خود را وارد کنید')])
    password = PasswordField(
        'رمز عبور:', validators=[DataRequired(
        message='لطفا رمز خود را وارد کنید'),
        Length(min=6,max=12,message=
        'رمز عبور باید بین ۶ تا ۱۲ کاراکتر طول داشته باشد')])
    password2 = PasswordField(
        'تکرار رمز عبور:',validators=[DataRequired(
        message='لطفا رمز خود را مجددا وارد کنید'),EqualTo(
        'password',message='هر دو فیلد رمز عبور باید برابر باشند')])
    submit = SubmitField('ثبت نام')
   
    def validate_firstname(self, first_name):
        user = User.query.filter_by(first_name=first_name.data).first()
        if user is not None:
            raise ValidationError('این نام قبلا ثبت شده است')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('این ایمیل قبلا ثبت شده است')

    def validate_phone(self, phone):
        user = User.query.filter_by(phone=phone.data).first()
        if user is not None:
            raise ValidationError('این شماره قبلا ثبت شده است')
        if len(phone.data) > 11 or len(phone.data) < 11:
            raise ValidationError('شماره موبایل معتبر نیست')
        if phone.data[0:2] != '09':
            raise ValidationError('شماره موبایل معتبر نیست')
        try:
            input_number = phonenumbers.parse(phone.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('شماره موبایل معتبر نیست')
        except:
            input_number = phonenumbers.parse("+98"+phone.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('شماره موبایل معتبر نیست')
    

class EmptyForm(FlaskForm):
    submit = SubmitField('Submit')


class ForgotForm(FlaskForm):
    email = StringField(
        'ایمیل:', validators=[DataRequired(message='لطفا ایمیل خود را وارد کنید'),
        Email(check_deliverability=True,message='ایمیل معتبر نیست')])
    submit = SubmitField('دریافت لینک')


class ResetPasswordForm(FlaskForm):
    password = PasswordField(
        'رمز عبور:', validators=[DataRequired(
        message='لطفا رمز خود را وارد کنید'),
        Length(min=6,max=12,message=
        'رمز عبور باید بین ۶ تا ۱۲ کاراکتر طول داشته باشد')])
    password2 = PasswordField(
        'تکرار رمز عبور:',validators=[DataRequired(
        message='لطفا رمز خود را مجددا وارد کنید'),EqualTo(
        'password',message='هر دو فیلد رمز عبور باید برابر باشند')])
    submit = SubmitField('تایید')
