# Authentication
This is a simple flask app that lets users register,login into the app.
the app supports reset password by sending a link to the user's email and take user to 
reset password form..
## used:
- python3 | flask framework
- SQLite database
## to use this app:
- add your mail info in config.py
- install python3, pip3, virtualenv
- clone the project 
- create a virtualenv named venv using ``` virtualenv -p python3 venv```
- Connect to virtualenv using ``` source venv/bin/activate ```
- from the project folder, install packages using ``` pip install -r 
requirements.txt ```
- for db migrations use : ``` flask db upgrade ```
- finally ``` flask run ```
## TODO:
- [ ] add "Edit Profile" option to the app
